# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150630123556) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "caches", force: true do |t|
    t.string   "request"
    t.text     "response"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "caches", ["request"], name: "index_caches_on_request", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "espetaculos", force: true do |t|
    t.string   "titulo"
    t.string   "genero"
    t.string   "nome_teatro"
    t.string   "cidade"
    t.string   "estado"
    t.string   "miniatura"
    t.string   "url"
    t.string   "data"
    t.integer  "relevancia"
    t.datetime "date_object"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "teatro_id"
    t.string   "keywords"
    t.integer  "codigo_cc"
    t.integer  "ordem"
    t.text     "description"
  end

  add_index "espetaculos", ["cidade"], name: "index_espetaculos_on_cidade", using: :btree
  add_index "espetaculos", ["codigo_cc"], name: "index_espetaculos_on_codigo_cc", using: :btree
  add_index "espetaculos", ["date_object"], name: "index_espetaculos_on_date_object", using: :btree
  add_index "espetaculos", ["estado"], name: "index_espetaculos_on_estado", using: :btree
  add_index "espetaculos", ["genero"], name: "index_espetaculos_on_genero", using: :btree
  add_index "espetaculos", ["keywords"], name: "index_espetaculos_on_keywords", using: :btree
  add_index "espetaculos", ["ordem"], name: "index_espetaculos_on_ordem", using: :btree
  add_index "espetaculos", ["relevancia"], name: "index_espetaculos_on_relevancia", using: :btree
  add_index "espetaculos", ["teatro_id"], name: "index_espetaculos_on_teatro_id", using: :btree

  create_table "espetaculos_genero_masters", id: false, force: true do |t|
    t.integer "genero_master_id"
    t.integer "espetaculo_id"
  end

  add_index "espetaculos_genero_masters", ["espetaculo_id"], name: "index_espetaculos_genero_masters_on_espetaculo_id", using: :btree
  add_index "espetaculos_genero_masters", ["genero_master_id"], name: "index_espetaculos_genero_masters_on_genero_master_id", using: :btree

  create_table "genero_masters", force: true do |t|
    t.string   "nome"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "handled_exceptions", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.text     "stacktrace"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "more_info"
  end

  create_table "teatros", force: true do |t|
    t.string   "nome"
    t.string   "cidade"
    t.string   "estado"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "endereco"
  end

  create_table "track_purchases", force: true do |t|
    t.integer  "number"
    t.string   "total_string"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "visores", force: true do |t|
    t.string   "imagem"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cache_image"
  end

  add_index "visores", ["url"], name: "index_visores_on_url", using: :btree

end
