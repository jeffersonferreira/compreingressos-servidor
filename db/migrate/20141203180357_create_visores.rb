class CreateVisores < ActiveRecord::Migration
  def change
    create_table :visores do |t|
      t.string :imagem
      t.string :url

      t.timestamps
    end
  end
end
