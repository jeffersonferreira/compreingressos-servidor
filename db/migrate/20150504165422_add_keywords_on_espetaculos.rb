class AddKeywordsOnEspetaculos < ActiveRecord::Migration
  def change
    add_column :espetaculos, :keywords, :string
    add_index  :espetaculos, :keywords
    Espetaculo.all.each {|x| x.save!}
  end
end
