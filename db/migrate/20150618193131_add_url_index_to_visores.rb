class AddUrlIndexToVisores < ActiveRecord::Migration
  def change
    add_index :visores, :url
  end
end
