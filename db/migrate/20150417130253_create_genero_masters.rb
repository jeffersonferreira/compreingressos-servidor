class CreateGeneroMasters < ActiveRecord::Migration
  def change
    create_table :genero_masters do |t|
      t.string :nome, index: true
      t.timestamps
    end

    create_table :espetaculos_genero_masters, id: false do |t|
    	t.belongs_to :genero_master, index: true
    	t.belongs_to :espetaculo, index: true
    end

    GeneroMaster.create(nome: 'Show')
    GeneroMaster.create(nome: 'Classicos')
    GeneroMaster.create(nome: 'Teatros')
  end
end
