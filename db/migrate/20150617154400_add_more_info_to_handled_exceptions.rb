class AddMoreInfoToHandledExceptions < ActiveRecord::Migration
  def change
    add_column :handled_exceptions, :more_info, :text
  end
end
