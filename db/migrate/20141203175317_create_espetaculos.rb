class CreateEspetaculos < ActiveRecord::Migration
  def change
    create_table :espetaculos do |t|

      # Mantendo todos as colunas compativeis com o ws do compreingressos para poder fazer
      # hotswap sem problemas no lado do app.
      t.string :titulo
      t.string :genero
      t.string :teatro
      t.string :cidade
      t.string :estado
      t.string :miniatura
      t.string :url
      t.string :data
      t.integer :relevancia

      t.datetime :date_object # objeto date com a data do evendo parseada

      t.timestamps
    end

    add_index :espetaculos, :genero
    add_index :espetaculos, :cidade
    add_index :espetaculos, :estado
    add_index :espetaculos, :relevancia
    add_index :espetaculos, :date_object
  end
end
