class CreateCaches < ActiveRecord::Migration
  def change
    create_table :caches do |t|
      t.string :request
      t.text :response

      t.timestamps
    end

    add_index :caches, :request
  end
end
