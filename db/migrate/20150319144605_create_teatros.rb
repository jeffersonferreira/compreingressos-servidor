class CreateTeatros < ActiveRecord::Migration
  def change
    create_table :teatros do |t|
      t.string :nome
      t.string :cidade
      t.string :estado
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
    add_column :espetaculos, :teatro_id, :integer
    add_index :espetaculos, :teatro_id
  end
end
