class AddCacheImageToVisores < ActiveRecord::Migration
  def change
    add_column :visores, :cache_image, :string
  end
end
