class ChangeTotalToTotalStringOnTrackPurchases < ActiveRecord::Migration
  def change
    rename_column :track_purchases, :total, :total_string
  end
end
