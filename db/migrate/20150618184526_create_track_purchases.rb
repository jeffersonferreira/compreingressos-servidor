class CreateTrackPurchases < ActiveRecord::Migration
  def change
    create_table :track_purchases do |t|
      t.integer :number
      t.string :total

      t.timestamps
    end
  end
end
