class ChangeTeatroToNomeTeatroOnEspetaculos < ActiveRecord::Migration
  def change
  	rename_column :espetaculos, :teatro, :nome_teatro
  end
end
