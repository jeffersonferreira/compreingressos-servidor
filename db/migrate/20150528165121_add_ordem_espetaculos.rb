class AddOrdemEspetaculos < ActiveRecord::Migration
  def change
    add_column :espetaculos, :ordem, :integer
    add_index  :espetaculos, :ordem
  end
end
