class AddCodigoCcToEspetaculos < ActiveRecord::Migration
  def change
    add_column :espetaculos, :codigo_cc, :int
    add_index :espetaculos, :codigo_cc
  end
end
