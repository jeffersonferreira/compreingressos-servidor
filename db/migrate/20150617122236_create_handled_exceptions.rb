class CreateHandledExceptions < ActiveRecord::Migration
  def change
    create_table :handled_exceptions do |t|
      t.string :title
      t.text :description
      t.text :stacktrace

      t.timestamps
    end
  end
end
