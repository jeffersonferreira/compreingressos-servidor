require 'rest_client'

class VisoresController < ApplicationController

  include Cacheable

  # Seguindo o padrão do WS do compreingressos para o caso de precisarmos fazer o
  # swap do nosso servidor com o deles.
  def lista
    cached_resource request
  end

  def sync_visores
    Delayed::Job.enqueue VisoresSyncJob.new
    respond_to do |format|
      format.html { render text: 'syncing' }
    end
  end
end
