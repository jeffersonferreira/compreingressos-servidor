require 'rest_client'

class EspetaculosController < ApplicationController

  include Cacheable

  ESPETACULOS_PATH = 'espetaculos.json'

  def index
    options            = {}
    options[:lat]      = params[:latitude]  if params[:latitude].present?
    options[:lng]      = params[:longitude] if params[:longitude].present?
    options[:page]     = params[:page]      if params[:page].present?
    options[:genre]    = params[:genero]    if params[:genero].present?
    options[:city]     = params[:cidade]    if params[:cidade].present?
    options[:keywords] = params[:keywords]  if params[:keywords].present?


    if options[:keywords].present?
      search_ci(options[:keywords])
    else
      espetaculos, total = Espetaculo.filter options

      respond_to do |format|
        format.json { render json: { espetaculos: espetaculos, total: total } }
      end

    end

  end

  def search_ci(keyword)
    url = "#{ENV['CI_HOST']}/#{ESPETACULOS_PATH}?key=#{ENV['CI_API_KEY']}&busca=#{keyword}"
    response = RestClient.get url
    espetaculos = []

    if response.code == 200
      response_json =  JSON.parse(response.body, symbolize_names: true)
      response_json.each do |hash|
        hash[:nome_teatro] = hash[:teatro]
        endereco = hash[:endereco]
        hash.delete :teatro
        hash.delete :endereco
        hash.delete :keywords
        espetaculos << hash
      end
      render json: {espetaculos: espetaculos, total: espetaculos.count}
    else
      puts "ERRO: #{response.code} URL: #{url}"
      puts response.body
    end
  end

  ##
  # Sincroniza os espetáculos com a compre ingressos ASSÍNCRONAMENTE.
  #
  # A sincronização ocorre da seguinte forma:
  # Todos os espetáculos são removidos.
  # Espetáculos são baixados do compreingressos.
  # Os espetáculos são separados entre os gêneros masters (Show Classicos e Teatros)
  # A localização dos teatros é consultada caso hajam teatros sem localização
  def sync_espetaculos
    Delayed::Job.enqueue EspetaculosSyncJob.new
    respond_to do |format|
      format.html { render text: 'syncing' }
    end
  end
end
