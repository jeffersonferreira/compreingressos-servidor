require 'rest_client'

class TrackPurchasesController < ApplicationController
  skip_before_action :verify_authenticity_token


  def create
    track_purchases = TrackPurchase.new
    track_purchases.number =  params[:number]
    track_purchases.total_string = params[:total]

    begin
      if track_purchases.save
        render json: { result: :ok }
      else
        render json: {result: track_purchases.errors.full_messages.to_s }
      end
    rescue => error
        render json: { error: error.message }, status: :unprocessable_entity
    end

  end
end