#encoding:utf-8

require 'rest_client'

class TicketsController < ApplicationController

  HOST     = ENV['CC_PRODUCTION_HOST']
  USER     = ENV['CC_USER']
  PASSWORD = ENV['CC_PASSWORD']
  ACTION   = 'getHistory'

  def index
    hash_client = URI::escape(params[:client_id])
    if hash_client
      env = params[:env].blank? ? :prod : params[:env].to_sym
      host, user, password = credentials(env)
      response = RestClient.post host, { user: user, password: password, action: ACTION, client_id: hash_client }, content_type: 'application/x-www-form-urlencoded'
      if response.code == 200
        result_hash = convert response, params[:os]
        render json: result_hash
      else
        render json: { error: response.code.to_s }
      end
    else
      render json: { error: 'client_id obrigatório' }
    end
  end

  private

    def credentials(env)
      if env == :prod
        [HOST, USER, PASSWORD]
      else
        ['http://186.237.201.132:81/compreingressos2/comprar/info.php', USER, '123123']
      end
    end

    ##
    # Recebe um json da C&C e converte para um json que nosso app entende.
    #
    # Formato do JSON recebido da C&C:
    # {
    #   "orders": [
    #     {
    #       "number": 436654,
    #       "assinatura": false,
    #       "pedido_pai": null,
    #       "date": "ter 2 jun",
    #       "total": "50,00",
    #       "items": [
    #         {
    #           "evento": "LOHENGRIN",
    #           "evento_id": 8346,
    #           "endereco": null,
    #           "nome_teatro": null,
    #           "data": "20\\/10\\/2015",
    #           "hora": "20h00",
    #           "setor": "SETOR 3 FOYER",
    #           "localizacao": "D-67",
    #           "tipo": "INTEIRA",
    #           "codigo": "0052411020200000100074",
    #           "valor_ingresso": "50,00"
    #         }
    #       ]
    #     }, ...
    # }
    def convert(response, os)
      os ||= 'ios'
      result_hash = []
      response_json = JSON.parse(response.body, symbolize_names: true)
      response_json[:orders].each do |hash|

        # Se items for null, então o pedido foi cancelado. Não confirmei isso com a
        # C&C mas é esse o comportamento que observei. Então iremos pular completamente
        # um item neste caso
        next if hash[:items].nil?

        if os == 'ios'
          separate_one_order_per_ticket(hash, result_hash)
        else
          separate_one_order_per_event(hash, result_hash)
        end

      end
      result_hash
    end

  def separate_one_order_per_ticket(hash, result_hash)
    qrcodes = hash[:items].map { |x| x[:codigo] }
    qrcodes.each do |qrcode|
      ingressos_hash = []
      item_by_qrcode = hash[:items].select { |x| x[:codigo] == qrcode }.first
      espetaculo_hash = espetaculo_hash(item_by_qrcode)
      ingressos_hash << ingresso_hash(hash, item_by_qrcode)
      result_hash << {
          number: hash[:number].to_s,
          date: get_formatted_event_date(item_by_qrcode),
          espetaculo: espetaculo_hash,
          ingressos: ingressos_hash
      }
    end
  end

  def separate_one_order_per_event(hash, result_hash)
    espetaculo_hash = {}
    event_ids = hash[:items].map { |x| x[:evento_id] }.uniq
    event_ids.each do |event_id|
      ingressos_hash = []
      items_by_event = hash[:items].select { |x| x[:evento_id] == event_id }
      items_by_event.each do |ingresso|
        espetaculo_hash = espetaculo_hash(ingresso)
        ingressos_hash << ingresso_hash(hash, ingresso)
      end
      result_hash << {
          number: hash[:number].to_s,
          date: get_formatted_event_date(items_by_event.first),
          espetaculo: espetaculo_hash,
          ingressos: ingressos_hash
      }
    end
  end

  def ingresso_hash(hash, ingresso)
    {
        qrcode: ingresso[:codigo],
        local: ingresso[:localizacao],
        type: ingresso[:tipo],
        price: ingresso[:valor_ingresso],
        service_price: '0,00',
        total: hash[:total]
    }
  end

    ##
  # Retona a date do evento formatada, e não a data de compra do ingresso.
  #
  # Como os ingresso são agrupados por evento, vamos pegar a data do primeiro evento
  # que estiver no array.
  def get_formatted_event_date(hash)
    date_str = hash[:data]
    date = DateTime.strptime(date_str, '%d/%m/%Y')
    I18n.localize(date, format: '%a %-d %b', locale: 'pt-BR').downcase
  end

  def espetaculo_hash(ingresso)
      evento = find_espetaculo(ingresso[:evento], ingresso[:evento_id])
      teatro = evento ? evento.teatro : nil
      {
        titulo:      ingresso[:evento],
        evento_id:   ingresso[:evento_id],
        endereco:    teatro ? teatro.endereco : nil,
        nome_teatro: teatro ? teatro.nome : nil,
        horario:     ingresso[:hora]
      }
    end

    ##
    # Encontra o espetáculo dado um evento_id.
    # E também leva em conta os eventos de homologação que estamos utilizando.
    # Estou buscando os eventos de homol no banco para termos uma idéia da velocidade.
    def find_espetaculo(titulo, evento_id)
      espetaculo = Espetaculo.where(codigo_cc: evento_id).first
      if !espetaculo && is_test_event(titulo)
        keywords   = Espetaculo.standardize_keywords(titulo)
        espetaculo = Espetaculo.where("espetaculos.keywords like '%#{keywords}%'").first
      end
      espetaculo
    end

    def is_test_event(titulo)
      titulo.include?('FAN TUT') || titulo.include?('LOHENGRIN')
    end
end