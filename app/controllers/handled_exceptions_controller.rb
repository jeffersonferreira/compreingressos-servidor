class HandledExceptionsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    exception = HandledException.new(params.require(:handled_exception).permit!)
    respond_to do |format|
      if exception.save
        format.json { render json: { handled_exception: exception } }
      else
        format.json { render json: { error: exception.errors.full_messages.to_s } }
      end
    end
  end
end