class Espetaculo < ActiveRecord::Base
	belongs_to :teatro
  has_and_belongs_to_many :genero_masters

  before_save :update_keywords

  PER_PAGE = 40
	LIMIT    = 100

	def distance
    attributes['distance'].to_i
  end	

  def as_json(options = nil)
    options ||= {}
    options[:except]  ||= [:created_at, :id, :keywords]
    options[:methods] ||= [:distance]
    super(options)
  end

	def self.filter(options = {})
    lat         = nil_coordinate_if_zero options[:lat]
    lng         = nil_coordinate_if_zero options[:lng]
    genre 			= options[:genre]
    city        = options[:city]
    radius      = options[:radius]
    page        = options[:page]
    keywords    = standardize_keywords options[:keywords]
    
    select_array  = ['espetaculos.*']
    select_array << "3959 * acos(cos(radians(#{lat})) * cos(radians(teatros.latitude)) * cos(radians(teatros.longitude) - radians(#{lng})) + sin(radians(#{lat})) * sin(radians(teatros.latitude))) * 1609.344 as distance" if lat && lng

    from_array  = %w(espetaculos teatros)
    from_array << %w(espetaculos_genero_masters genero_masters) if genre

    where_array  = ['espetaculos.teatro_id = teatros.id']
    where_array << ['espetaculos_genero_masters.espetaculo_id = espetaculos.id'] if genre
    where_array << ['espetaculos_genero_masters.genero_master_id = genero_masters.id'] if genre
    where_array << ["genero_masters.nome = '#{genre}'"] if genre
    where_array << ["espetaculos.keywords like '%#{keywords}%'"] if keywords

   	# Código para filtrar estabelecimentos por raio. Lembrando que a função earth_box pode retornar estabelecimentos mais longe do que o raio fornecido.
    # where_array << "earth_box(ll_to_earth(#{lat},#{lng}), #{radius}) @> ll_to_earth(addresses.latitude, addresses.longitude)" if lat && lng && radius   

    select_piece = "select #{select_array.join(',')} "
    from_piece   = "from #{from_array.join(',')} "
    where_piece  = "where #{where_array.join(' and ')} "

    query = select_piece << from_piece << where_piece

    if lat && lng
      query << 'order by distance asc, espetaculos.titulo '
    else
      query << 'order by espetaculos.ordem '
    end

    query << "limit #{PER_PAGE} offset #{offset_for_page(page)}" if page
    query << "limit #{LIMIT}" unless page

    puts '-------- ' + query

    espetaculos = Espetaculo.find_by_sql(query)
    total = filter_count(options)

    [espetaculos, total]
  end

  def self.standardize_keywords(keywords)
    I18n.transliterate(keywords).downcase if keywords
  end

  def update_keywords
    self.keywords = Espetaculo.standardize_keywords(self.titulo)
    self.keywords << ' '
    self.keywords << Espetaculo.standardize_keywords(self.cidade)
  end

	private

    def self.nil_coordinate_if_zero(coordinate)
      if coordinate && (coordinate == '0.0' || coordinate == '0')
        nil
      else
        coordinate
      end
    end

    def self.offset_for_page(page)
      page.to_i * PER_PAGE
    end

    def self.filter_count(options = {})
	    lat         = options[:lat]
    	lng         = options[:lng]
    	genre 		  = options[:genre]
    	city        = options[:city]
    	radius      = options[:radius]
    	page        = options[:page]
      keywords    = standardize_keywords options[:keywords]

	    from_array  =  %w(espetaculos teatros)
      from_array  << %w(espetaculos_genero_masters genero_masters) if genre
	    where_array =  ['espetaculos.teatro_id = teatros.id']
	    where_array << ['espetaculos_genero_masters.espetaculo_id = espetaculos.id'] if genre
      where_array << ['espetaculos_genero_masters.genero_master_id = genero_masters.id'] if genre
      where_array << ["genero_masters.nome = '#{genre}'"] if genre
      where_array << ["espetaculos.keywords like '%#{keywords}%'"] if keywords
	    # where_array << "earth_box(ll_to_earth(#{lat},#{lng}), #{radius}) @> ll_to_earth(addresses.latitude, addresses.longitude)" if lat && lng && radius   
	    count_query = "select count(*) from #{from_array.join(',')} where #{where_array.join(' and ')}"
	    result = ActiveRecord::Base.connection.execute(count_query).values.first.first
	    result.to_i
  end
end
