class Teatro < ActiveRecord::Base

	scope :coordinateless, lambda { where('latitude is null') }

	def geocoder_desc
		"#{self.nome}, #{self.cidade}, #{self.estado}, Brasil"
	end

	def query_coordinates!
		coordinates = Geocoder.coordinates geocoder_desc
		self.latitude, self.longitude = coordinates
		save!
	end

	def has_coordinates?
		self.latitude != nil && self.longitude != nil
	end
end
