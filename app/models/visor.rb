class Visor < ActiveRecord::Base

  mount_uploader :cache_image, CacheImageUploader

  def self.process_new_entries(json)
    remove_old_entries json
    create_or_update_entries json
  end

  def self.remove_old_entries(json)
    Visor.all.each do |visor|
      json_not_contains_visor = json.select { |entry| entry[:url] == visor.url }.empty?
      visor.destroy if json_not_contains_visor
    end
  end

  def self.create_or_update_entries(json)
    json.each do |entry|
      existing = Visor.where(url: entry[:url]).first
      if existing
        existing.update_image_if_needed(entry)
      else
        create_visor(entry)
      end
    end
  end

  def self.create_visor(entry)
    puts ' ========================>>>>>> PROCESSING ' + entry[:imagem]
    visor = Visor.new(entry)
    visor.remote_cache_image_url = visor.imagem
    visor.save!
  end

  def update_image_if_needed(entry)
    update_attribute(:imagem, entry[:imagem]) if self.imagem != entry[:imagem]
  end

  def image_for_options(options)
    options[:width] ||= 0
    options[:os]    ||= 'ios'
    width = options[:width].to_i
    if ios?(options[:os])
      version = ios_version_for_width(width)
    else
      version = android_version_for_width(width)
    end
    version << '_3g' if options[:con] == 'wwan'

    # A versão mobile_high é igual a imagem original
    if version == 'mobile_high'
      self.cache_image.url
    else
      self.cache_image.url(version.to_sym)
    end

  end

  def android_version_for_width(width)
    version = ''
    if width <= 240
      version << 'mobile_low'
    elsif width > 240 && width < 720
      version << 'mobile_medium'
    else
      version << 'mobile_high'
    end
    version
  end

  def ios_version_for_width(width)
    version = ''
    if width >= 414
      version << 'mobile_high'
    else
      version << 'mobile_medium'
    end
    version
  end

  def ios?(os)
    os == 'ios'
  end
end
