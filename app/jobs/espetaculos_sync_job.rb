require 'rest_client'

class EspetaculosSyncJob

  ESPETACULOS_PATH = 'espetaculos.json'
  GENEROS_MASTER   = %w(Show Classicos Teatros)

  def perform
    renew_espetaculos
    fill_generos_master
    query_coordinates
  end

  def host
    ENV['CI_HOST']
  end

  def api_key
    ENV['CI_API_KEY']
  end

  ##
  # Limpa a base de espetáculos e atualiza com os do compreingressos
  def renew_espetaculos
    url = "#{host}/#{ESPETACULOS_PATH}?key=#{api_key}"
    response = RestClient.get url
    if response.code == 200
      process_espetaculos_response response
    else
      puts "ERRO: #{response.code} URL: #{url}"
      puts response.body
    end
  end

  def process_espetaculos_response(response)
    Espetaculo.destroy_all
    response_json = JSON.parse(response.body, symbolize_names: true)
    response_json.each_with_index do |hash, index|
      hash[:nome_teatro] = hash[:teatro]
      endereco = hash[:endereco]
      hash.delete :teatro
      hash.delete :endereco

      # Temos que ignorar as keywords vindas da compreingressos pois já usamos esse
      # campo internamente. Em breve iremos redirecionar a busca para o sistem deles
      # então não precisaremos mais desse campo internamente.
      hash.delete :keywords

      espetaculo = Espetaculo.new(hash)
      espetaculo.ordem = index
      teatro_hash = {
          nome:   espetaculo.nome_teatro,
          cidade: espetaculo.cidade,
          estado: espetaculo.estado,
      }
      teatro = Teatro.where(teatro_hash).first
      if teatro
        teatro.endereco = endereco
        teatro.save
      else
        teatro = Teatro.create(teatro_hash)
      end
      espetaculo.teatro = teatro
      espetaculo.save
    end
  end

  ##
  # Para cada genero master, realiza a consulta dos espetaculos no
  # compreingressos e preenche o campo genero_master na nossa base
  def fill_generos_master
    GENEROS_MASTER.each do |genero_master|
      puts "FILLING #{genero_master}"
      fill GeneroMaster.find_by_nome(genero_master)
    end
  end

  def fill(genero_master)
    url = "#{host}/#{ESPETACULOS_PATH}?key=#{api_key}&genero=#{genero_master.nome}"
    response = RestClient.get url
    if response.code == 200
      response_json = JSON.parse(response.body, symbolize_names: true)
      response_json.each do |hash|
        espetaculo = Espetaculo.find_by_miniatura!(hash[:miniatura]) # A miniatura contém o código do espetáculo
        espetaculo.genero_masters << genero_master
      end
    else
      puts "ERRO: #{response.code} URL: #{url}"
      puts response.body
    end
  end

  def query_coordinates
    ActiveRecord::Base.transaction do
      Teatro.coordinateless.each do |teatro|
        teatro.query_coordinates!
      end
    end
  end
end