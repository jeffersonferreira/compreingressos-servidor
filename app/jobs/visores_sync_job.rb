require 'rest_client'

class VisoresSyncJob

  VISORES_PATH = 'visores/lista.json'

  def perform
    url = "#{ENV['CI_HOST']}/#{VISORES_PATH}?key=#{ENV['CI_API_KEY']}"
    response = RestClient.get url

    if response.code == 200
      json = JSON.parse(response.body, symbolize_names: true)
      Visor.process_new_entries(json)
    else
      puts "ERRO: #{response.code} URL: #{url}"
      puts 'response.body'
    end
  end

end