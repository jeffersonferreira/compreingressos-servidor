module Cacheable
  API_KEY         = '8435D5115e46a70i6648715850882eus'
  HOST            = 'http://www.compreingressos.com'
  MAX_AGE_SECONDS = ENV['MAX_AGE_SECONDS'].to_f

  def max_age_seconds
    MAX_AGE_SECONDS
  end

  def cached_resource(request)

    fullpath = request.original_fullpath

    cached = Cache.where(request: fullpath).first
    if cached
      if (Time.now - cached.updated_at) < max_age_seconds
        puts '####### HIT!'
        respond_to do |format|
          format.json { render text: cached.response }
        end
        return
      else
        puts '####### OLD!'
      end
    end

    puts '####### MISS!'

    response = RestClient.get url_with_key(fullpath)
    response_body = response.body.gsub("\r\n", ' ')

    # Vamos substituir o link do banner de acordo com o perfil da requisição
    if fullpath.include? 'visores/lista'
      response_body = switch_images response_body, { os: params[:os], width: params[:width], con: params[:con] }
    end

    if cached
      cached.update_attribute(:response, response_body)
      # Não sei porque o update_attribute e o update_attributes não estão atualizando o updated_at
      cached.touch
    else
      Cache.create(request: fullpath, response: response_body)
    end
    respond_to do |format|
      format.json { render text: response_body }
    end
  end

  private

    def switch_images(response, options)
      json_response = JSON.parse(response, symbolize_names: true)
      json_response.each do |entry|
        visor = Visor.where(url: entry[:url]).first
        if visor
          # Se retornar um visor, vamos trocar a url da imagem de acordo com o perfil
          entry[:imagem] = visor.image_for_options(options)
        else
          # Se não retornar um visor, é porque os visores ainda não foram sincronizados
          # com a compreingressos. Neste caso, se a conexão for 3g/4g, vamos remover o
          # link da imagem para não correr o risco do usuário não conseguir baixar.
          # Se for wifi, vamos deixar rolar porque se estiver no perfil incorreto, o max
          # que vai acontecer é disperdiçar alguns kb da franquia da NET do usuário ;P
          if options[:con] == 'wwan'
            entry[:imagem] = ''
          end
        end
      end
      json_response.to_json
    end

    def remove_query_string(path)
      path.split('?')[0]
    end

    def url_with_key(path)
      url = "#{HOST}#{path}"
      if path.include?('?')
        url << '&key='
      else
        url << '?key='
      end
      url << API_KEY
    end
end